<script type="text/javascript">
    jQuery(document).ready(function($) {
        if (typeof $.fn.imageMask != "undefined" && Modernizr.inlinesvg && Modernizr.svgclippaths) {
            $(".im-gallery").each(function() {
                var $this = $(this);
                $(".gallery-item img").imageMask({
                    mask: $this.attr("data-im-mask"),
                    spacing: $this.attr("data-im-spacing"),
                    border_size: $this.attr("data-im-border-size"),
                    border_color: $this.attr("data-im-border-color"),
                    shadow_size: $this.attr("data-im-shadow-size"),
                    shadow_opacity: $this.attr("data-im-shadow-opacity")
                });
            });
<?php foreach ($options["th"] as $size => $config): ?>
            $(".attachment-<?=$size?>").imageMask({
                mask: '<?=$config["mask"]?>',
                border_size: <?=$config['border']['size']?>,
                border_color: '<?=$config['border']['color']?>',
                shadow_size: <?=$config['shadow']['size']?>,
                shadow_opacity: <?=$config['shadow']['opacity']?>

            });
<?php endforeach; ?>
        }
    });
</script>
