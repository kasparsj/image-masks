<?php if (apply_filters('use_default_gallery_style', true)): ?>
<style type="text/css">
    .im-gallery-group {
        float: <?=$float?>;
    }
    .im-gallery .gallery-item {
        float: <?=$float?>;
        line-height: 0;
        margin: 0 <?=$spacing?>px <?=$spacing?>px 0;
    }
<?php if ($layout != "alternately"): ?>
<?php if ($mask == "parlgrm-left" || $mask == "parlgrm-right"): ?>
    .im-gallery .gallery-item {
        margin-<?=$float?>: -<?=10-$spacing/2?>px;
    }
<?php endif; ?>
<?php else: // alternately ?>
<?php if ($mask == "parlgrm-left" || $mask == "parlgrm-right"): ?>
    .im-gallery .gallery-item {
        float: none;
        margin-<?=$float?>: -10px;
    }
<?php elseif ($mask == "ellipse" || $mask == "rhombus"): ?>
    .im-gallery {
        padding-top: <?=$height/2?>px;
        margin-<?=$float?>: <?=$width/2?>px;
        max-width: <?=$width+($columns-1)*($width/4*3+$spacing)?>px;
    }
    .im-gallery-group {
        margin-<?=$float?>: -<?=$width/2?>px;
    }
    .im-gallery .gallery-item {
        margin: <?=$spacing?>px <?=$spacing?>px;
        margin-<?=$float?>: -<?=$width/2-$spacing/2?>px;
    }
    .im-gallery .gallery-item:first-child {
        transform: translateY(-<?=$height/2?>px);
        -ms-transform: translateY(-<?=$height/2?>px);
        -o-transform: translateY(-<?=$height/2?>px);
        -moz-transform: translateY(-<?=$height/2?>px);
        -webkit-transform: translateY(-<?=$height/2?>px);
        margin: 0 <?=$spacing?>px;
        margin-<?=$float?>: 0;
    }
<?php elseif ($mask == "hex-hor"): ?>
    .im-gallery {
        padding-top: <?=$height/2+$border_size?>px;
        margin-<?=$float?>: <?=$width/4+$border_size?>px;
        max-width: <?=$total_width+($columns-1)*($total_width/4*3+$spacing)?>px;
    }
    .im-gallery-group {
        margin-<?=$float?>: -<?=$width/4+$border_size+$shadow_size?>px;
    }
    .im-gallery .gallery-item {
        margin: <?=$spacing?>px <?=$spacing?>px;
        margin-<?=$float?>: -<?=$width/4-$spacing/2+$border_size+$shadow_size?>px !important;
    }
    .im-gallery .gallery-item:first-child {
        transform: translateY(-<?=$height/2+$border_size+$shadow_size?>px);
        -ms-transform: translateY(-<?=$height/2+$border_size+$shadow_size?>px);
        -o-transform: translateY(-<?=$height/2+$border_size+$shadow_size?>px);
        -moz-transform: translateY(-<?=$height/2+$border_size+$shadow_size?>px);
        -webkit-transform: translateY(-<?=$height/2+$border_size+$shadow_size?>px);
        margin: 0 <?=$spacing?>px;
        margin-<?=$float?>: 0 !important;
    }
<?php elseif ($mask == "hex-ver"): ?>
    .im-gallery {
        margin-top: -<?=$height/2+$spacing?>px;
        margin-<?=$float?>: <?=$width/2?>px;
        max-width: <?=$width+($columns-1)*($width/4*3+$spacing)?>px;
    }
    .im-gallery-group {
        margin-<?=$float?>: -<?=$width/2?>px;
        margin-top: <?=$height/2?>px;
    }
    .im-gallery .gallery-item {
        margin: 0 <?=$spacing?>px;
        margin-<?=$float?>: -<?=$width/2-$spacing/2?>px;
        transform: translateY(<?=$height/4*3+$spacing*2?>px);
        -ms-transform: translateY(<?=$height/4*3+$spacing*2?>px);
        -o-transform: translateY(<?=$height/4*3+$spacing*2?>px);
        -moz-transform: translateY(<?=$height/4*3+$spacing*2?>px);
        -webkit-transform: translateY(<?=$height/4*3+$spacing*2?>px);
    }
    .im-gallery .gallery-item:first-child {
        margin-<?=$float?>: 0;
        margin: <?=$spacing?>px 0;
        transform: none;
        -ms-transform: none;
        -o-transform: none;
        -moz-transform: none;
        -webkit-transform: none;
    }
<?php elseif ($mask == "tri-hex"): ?>
    .im-gallery {
        padding-top: <?=$height/4?>px;
        margin-<?=$float?>: <?=$width/7?>px;
        max-width: <?=$width+($columns-1)*($width/7*6+$spacing)?>px;
    }
    .im-gallery-group {
        margin-top: -<?=$height/4?>px;
        margin-<?=$float?>: -<?=$width/7?>px;
    }
    .im-gallery .gallery-item {
        margin: <?=$spacing?>px <?=$spacing?>px;
        margin-top: <?=$height/4*3+$spacing?>px;
        margin-<?=$float?>: -<?=$width+$spacing?>px;
    }
    .im-gallery .gallery-item:first-child {
        margin-top: 0;
        margin-<?=$float?>: 0;
    }
<?php endif; ?>
<?php endif; ?>
</style>
<?php endif; ?>
<?php
$attrs  = sprintf('data-im-mask="%s"', $mask);
if ($spacing)
    $attrs .= sprintf('data-im-spacing="%d"', $spacing);
if ($border_size) {
    $attrs .= sprintf('data-im-border-size="%d"', $border_size);
    $attrs .= sprintf('data-im-border-color="%s"', $border['color']);
}
if ($shadow_size) {
    $attrs .= sprintf('data-im-shadow-size="%d"', $shadow_size);
    $attrs .= sprintf('data-im-shadow-opacity="%d"', $shadow['opacity']);
}
?>
<div id="im-gallery-<?=$instance?>" class="im-gallery im-gallery-<?=sanitize_html_class($mask)?> galleryid-<?=$id?> gallery-columns-<?=$columns?> gallery-size-<?=sanitize_html_class($size)?>"<?=$attrs?>>
<?php for ($group=0; $group<ceil(count($attachments)/$group_size); $group++): ?>
    <div id="im-gallery-group-<?=$group?>" class="im-gallery-group">
        <?php for ($i=($group*$group_size); $i<min($group*$group_size+$group_size, count($attachments)); $i++): $attachmentID = $attachments[$i]->ID; ?>
        <<?=$itemtag?> class="gallery-item">
            <?=isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($attachmentID, $size, false, false) : wp_get_attachment_link($attachmentID, $size, true, false);?>
        </<?=$itemtag?>>
        <?php endfor; ?>
    </div>
<?php endfor; ?>
    <br style="clear:both" />
</div>