<div class="wrap">
    <div class="icon32" id="icon-options-general"><br></div>
    <h2>Image Masks</h2>
    <!--Some optional text here explaining the overall purpose of the options and what they relate to etc.-->
    <form method="post" action="options.php">
        <?php settings_fields('image_masks'); ?>
        <h3>Galleries</h3>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label>Image mask:</label></th>
                    <td>
                        <input type="radio" id="im_mask_off" name="image_masks[mask]" value=""<?=$options["mask"] == $mask ? ' checked="checked"' : ''?> />
                        <label for="im_mask_off">off</label>&nbsp;
                        <?php foreach ($masks as $mask): ?>
                        <input type="radio" id="im_mask_<?=$mask?>" name="image_masks[mask]" value="<?=$mask?>"<?=$options["mask"] == $mask ? ' checked="checked"' : ''?> />
                        <label for="im_mask_<?=$mask?>"><?=$mask?></label>&nbsp;
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr valign="top" id="im_size_row">
                    <th scope="row"><label for="im_size">Image size:</label></th>
                    <td>
                        <select id="im_size" name="image_masks[size]">
                            <?php foreach ($sizes as $size => $title): if ($title == "custom" && !function_exists("add_image_size")) continue; ?>
                            <option value="<?php echo $size ?>"<?php echo $size == $options["size"] ? ' selected="selected"' : '' ?>><?php echo $title ?></option>
                            <?php endforeach; ?>
                        </select>
                        &nbsp;
                        <span id="im_size_custom"<?php echo $options["size"] != "im-gallery-thumb" ? ' style="display:none"' : ''?>>
                            <label for="im_custom_width">Width:</label> <input id="im_custom_width" name="image_masks[custom_width]" size="4" type="text" value="<?php echo $options["custom_width"] ?>" />
                            <label for="im_custom_height">Height:</label> <input id="im_custom_height" name="image_masks[custom_height]" size="4" type="text" value="<?php echo $options["custom_height"] ?>" />
                        </span>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label>Layout:</label></th>
                    <td>
                        <input type="radio" id="im_layout_standard" name="image_masks[layout]" value=""<?=$options["layout"]=="" ? ' checked="checked"' : '' ?> />
                        <label for="im_layout_standard">Standard</label>
                        <input type="radio" id="im_layout_alternately" name="image_masks[layout]" value="alternately"<?=$options["layout"]=="alternately" ? ' checked="checked"' : '' ?> />
                        <label for="im_layout_alternately">Alternately</label>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="im_spacing">Spacing:</label></th>
                    <td><input class="small-text" id="im_spacing" name="image_masks[spacing]" value="<?php echo $options["spacing"] ?>" type="number" step="1" />px</td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="im_border_size">Border:</label></th>
                    <td>
                        <input class="small-text" id="im_border_size" name="image_masks[border][size]" value="<?php echo $options["border"]["size"] ?>" type="number" step="1" min="0" />px 
                        <input id="im_border_color" name="image_masks[border][color]" value="<?php echo $options["border"]["color"] ?>" type="text" data-default-color="#000000" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="im_shadow_size">Shadow:</label></th>
                    <td>
                        <input class="small-text" id="im_shadow_size" name="image_masks[shadow][size]" value="<?php echo $options["shadow"]["size"] ?>" type="number" step="1" min="0" />px 
                        <label for="im_shadow_opacity">opacity:<label> <input class="small-text" id="im_shadow_opacity" name="image_masks[shadow][opacity]" value="<?php echo $options["shadow"]["opacity"] ?>" type="number" step="1" min="0" max="100" />% 
<!--                        <label for="im_shadow_dx">dx:<label> <input class="small-text" id="im_shadow_dx" name="image_masks[shadow][dx]" value="<?php echo $options["shadow"]["dx"] ?>" type="number" step="1" min="0" max="100" /> 
                        <label for="im_shadow_dy">dy:<label> <input class="small-text" id="im_shadow_dy" name="image_masks[shadow][dy]" value="<?php echo $options["shadow"]["dy"] ?>" type="number" step="1" min="0" max="100" /> -->
                    </td>
                </tr>
            </tbody>
        </table>
        <h3>Thumbnails</h3>
        <table class="form-table">
            <tbody>
                <?php foreach ($sizes as $size): ?>
                <tr valign="top">
                    <th scope="row"><label><?=$size?></label></th>
                    <td>
                        <input type="radio" id="im_th_<?=$size?>_off" name="image_masks[th][<?=$size?>][mask]" value=""<?=!isset($options["th"][$size]) || $options["th"][$size]["mask"] == "" ? ' checked="checked"' : ''?> />
                        <label for="im_th_<?=$size?>_off">off</label>&nbsp;
                        <?php foreach ($masks as $mask): ?>
                        <input id="im_th_<?=$size?>_<?=$mask?>" name="image_masks[th][<?=$size?>][mask]" value="<?=$mask?>" type="radio"<?=isset($options["th"][$size]) && $options["th"][$size]["mask"] == $mask ? ' checked="checked"' : ''?> />
                        <label for="im_th_<?=$size?>_<?=$mask?>"><?=$mask?></label>&nbsp;
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="im_th_<?=$size?>_border_size">Border:</label></th>
                    <td>
                        <input class="small-text" id="im_th_<?=$size?>_border_size" name="image_masks[th][<?=$size?>][border][size]" value="<?php echo intval(@$options["th"][$size]["border"]["size"]) ?>" type="number" step="1" min="0" />px
                        <input id="im_th_<?=$size?>_border_color" name="image_masks[th][<?=$size?>][border][color]" value="<?php echo $options["th"][$size]["border"]["color"] ?>" type="text" data-default-color="#000000" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="im_th_<?=$size?>_shadow_size">Shadow:</label></th>
                    <td>
                        <input class="small-text" id="im_th_<?=$size?>_shadow_size" name="image_masks[th][<?=$size?>][shadow][size]" value="<?php echo intval(@$options["th"][$size]["shadow"]["size"]) ?>" type="number" step="1" min="0" />px
                        <label for="im_th_<?=$size?>_shadow_opacity">opacity:<label> <input class="small-text" id="im_th_<?=$size?>_shadow_opacity" name="image_masks[th][<?=$size?>][shadow][opacity]" value="<?php echo isset($options["th"][$size]["shadow"]["opacity"]) ? $options["th"][$size]["shadow"]["opacity"] : 75 ?>" type="number" step="1" min="0" max="100" />%
<!--                        <label for="im_th_<?=$size?>_shadow_dx">dx:<label> <input class="small-text" id="im_th_<?=$size?>_shadow_dx" name="image_masks[th][<?=$size?>][shadow][dx]" value="<?php echo $options["th"][$size]["shadow"]["dx"] ?>" type="number" step="1" min="0" max="100" />
                        <label for="im_th_<?=$size?>_shadow_dy">dy:<label> <input class="small-text" id="im_th_<?=$size?>_shadow_dy" name="image_masks[th][<?=$size?>][shadow][dy]" value="<?php echo $options["th"][$size]["shadow"]["dy"] ?>" type="number" step="1" min="0" max="100" /> -->
                    </td>
                </tr>
                <?php endforeach; ?>
                <tr valign="top">
                    <th scope="row"><label>all other thumbnails</label></th>
                    <td>
                        <input type="radio" id="im_th_other_off" name="image_masks[th][other][mask]" value=""<?=!isset($options["th"]["other"]) || $options["th"]["other"]["mask"] == "" ? ' checked="checked"' : ''?> />
                        <label for="im_th_other_off">off</label>&nbsp;
                        <?php foreach ($masks as $mask): ?>
                        <input id="im_th_other_<?=$mask?>" name="image_masks[th][other][mask]" value="other" type="radio"<?=isset($options["th"]["other"]) && $options["th"]["other"]["mask"] == $mask  ? ' checked="checked"' : ''?> />
                        <label for="im_th_other<?=$mask?>"><?=$mask?></label>&nbsp;
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="im_th_other_border_size">Border:</label></th>
                    <td>
                        <input class="small-text" id="im_th_other_border_size" name="image_masks[th][other][border][size]" value="<?php echo intval(@$options["th"]["other"]["border"]["size"]) ?>" type="number" step="1" min="0" />px
                        <input id="im_th_other_border_color" name="image_masks[th][other][border][color]" value="<?php echo $options["th"]["other"]["border"]["color"] ?>" type="text" data-default-color="#000000" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="im_th_other_shadow_size">Shadow:</label></th>
                    <td>
                        <input class="small-text" id="im_th_other_shadow_size" name="image_masks[th][other][shadow][size]" value="<?php echo intval(@$options["th"]["other"]["shadow"]["size"]) ?>" type="number" step="1" min="0" />px
                        <label for="im_th_other_shadow_opacity">opacity:<label> <input class="small-text" id="im_th_other_shadow_opacity" name="image_masks[th][other][shadow][opacity]" value="<?php echo isset($options["th"]["other"]["shadow"]["opacity"]) ? $options["th"]["other"]["shadow"]["opacity"] : 75 ?>" type="number" step="1" min="0" max="100" />%
<!--                        <label for="im_th_other_shadow_dx">dx:<label> <input class="small-text" id="im_th_other_shadow_dx" name="image_masks[th][other][shadow][dx]" value="<?php echo $options["th"]["other"]["shadow"]["dx"] ?>" type="number" step="1" min="0" max="100" />
                        <label for="im_th_other_shadow_dy">dy:<label> <input class="small-text" id="im_th_other_shadow_dy" name="image_masks[th][other][shadow][dy]" value="<?php echo $options["th"]["other"]["shadow"]["dy"] ?>" type="number" step="1" min="0" max="100" /> -->
                    </td>
                </tr>
            </tbody>
        </table>
        <h3>Restore</h3>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><label for="im_restore">Restore to default settings:</label></th>
                    <td><input id="im_restore" name="image_masks[restore]" type="checkbox" /></td>
                </tr>
            </tbody>
        </table>
        <?php submit_button(); ?>
    </form>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#im_size").change(function() {
            if (this.value == "im-gallery-thumb")
                $("#im_size_custom").show();
            else
                $("#im_size_custom").hide();
        });
        $("[data-default-color]").wpColorPicker({
            width: 200
        });
    });
</script>