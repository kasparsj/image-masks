<defs>
    <?php echo $this->get_shapes($id, $mask, $layout, $width, $height, $border, $shadow) ?>
    <?php echo $this->get_filters($id, $mask, $layout, $shadow); ?>
<?php if (@$shadow["size"]): ?>
<?php endif; ?>
</defs>
<g>
    <clipPath id="im-<?=$id?>-clippath">
        <use xlink:href="#im-<?=$id?>" />
    </clipPath>
<?php if ($layout == "alternately" && in_array($mask, self::$masks_alt)): ?>
    <clipPath id="im-<?=$id?>-alt-clippath">
        <use xlink:href="#im-<?=$id?>-alt" />
    </clipPath>
<?php endif; ?>
</g>
