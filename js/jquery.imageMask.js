/**
 * jQuery imageMask
 * @author Kaspars Jaudzems - kasparsj@gmail.com
 * @see https://github.com/kasparsj/imageMask
 * 
 * @version 1.0
 * @license MIT License
 */

(function($) {
  var $_count = 0;
      $_masks = ["parlgrm-left", "parlgrm-right", "ellipse", "rhombus", "hex-hor", "hex-ver"],
      $_loadlist = [],
      $_defs = {},
      $_interval = setInterval(poll, 0);
  
  $.fn.imageMask = function(options) {
    if (!this.is("img"))  return false;
    
    options = $.extend({
        mask: 'parlgrm-left',
        border_size: 0,
        border_color: "#000000",
        shadow_size: 0,
        shadow_opacity: 0.75
    }, options || {});
    
    if (!jQuery.inArray(options.mask, $_masks))
        options.mask = $_masks[0];
    options.border_size = parseInt(options.border_size);
    options.shadow_size = parseInt(options.shadow_size);
    options.shadow_opacity = parseFloat(options.shadow_opacity);
    options.shadow_opacity = options.shadow_opacity > 1 ? options.shadow_opacity/100 : options.shadow_opacity;
    
    this.css("visibility", "hidden");
    $_loadlist.push([this, options]);
    poll();
    
    return this;
  };
  
  function poll() {
      var options;
      for (var i=0; i<$_loadlist.length; i++) {
        options = $_loadlist[i][1];
        $_loadlist[i][0].each(function() {
            if (this.width && this.height) {
                var $target = $(this),
                    $parent = $(this).parent(),
                    $link = null,
                    id = options.mask+'-'+this.width+'x'+this.height,
                    defs = get_defs(id, this.width, this.height, options);
                if ($parent.prop("tagName") === 'A')
                    $target = $link = $parent;
                if (defs)
                    $(document.body).append(defs);
                $target.replaceWith(get_thumbnail($link, this.src, id, this.width, this.height, options));
                $_loadlist[i][0] = $_loadlist[i][0].not(this);
            }
        });
        if (!$_loadlist[i][0].length)
            $_loadlist.splice(i, 1);
      }
  }
  
  function get_defs(id, width, height, options) {
      var defs = '';
      if (typeof $_defs[id] === "undefined") {
          $_defs[id] = -1;
          defs = '\
<div id="#imageMask-defs">\n\
<svg width="0" height="0" xmlns="http://www.w3.org/2000/svg">\n\
    <defs>' +
    get_shapes(id, width, height, options) +
    get_filters(id, width, height, options) +
'   </defs>\n\
    <g>\n\
        <clipPath id="im-'+id+'-clippath">\n\
            <use xlink:href="#im-'+id+'" />\n\
        </clipPath>\n\
    </g>\n\
</svg>\n\
</div>';
      }
      $_defs[id]++;
      return defs;
  }

  function get_thumbnail($link, src, id, width, height, options) {
      var svg_width = (width+options.border_size+options.shadow_size),
          svg_height = (height+options.border_size+options.shadow_size),
          image = get_image(src, id, width, height, options);
      if ($link)
        image = get_link($link, image);
      var svg = '\
<svg width="'+svg_width+'" height="'+svg_height+'" viewBox="0 0 '+svg_width+' '+svg_height+'" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">' +
    image +
'</svg>';
      return svg;
  }
  
  function get_image(src, id, width, height, options) {
      var x = options.border_size + options.shadow_size,
          y = options.border_size + options.shadow_size,
          image = '<image clip-path="url(#im-'+id+'-clippath)" width="'+width+'" height="'+height+'" xlink:href="'+src+'" x="'+x+'" y="'+y+'" />';
      if (options.border_size)
          image += '<use xlink:href="#im-'+id+'-border" filter="url(#im-'+id+'-shadow)" />';
      return image;
  }

  function get_link($link, image) {
    var attrs = '';
    $(['href', 'title', 'role']).each(function() {
        if ($link.attr(this))
            attrs += ' '+this+'="'+$link.attr(this)+'"';
    });
    return '<a"' + attrs + '">' + image + '</a>';
  }
  
  function get_shapes(id, width, height, options) {
      var shapes = get_shape(id, width, height, options);
      if (options.border_size)
          shapes += get_shape(id, width, height, options, true);
      return shapes;
  }

  function get_shape(id, width, height, options, is_border) {
      var dx, dy,
          fill = 'black',
          atts = '';
      dx = dy = options.border_size;
      if (is_border && options.border_size) {
          dx = dy = options.border_size/2;
          width += options.border_size/2;
          height += options.border_size/2;
          fill = 'none';
          atts = ' stroke="'+options.border_color+'" stroke-width="'+options.border_size+'"';
      }
      if (options.shadow_size) {
          dx += options.shadow_size;
          dy += options.shadow_size;
      }
      var points;
      switch (options.mask) {
          default:
          case 'parlgrm-left':
              points = [
                  dx+','+dy,
                  (width+dx-10)+','+dy,
                  (width+dx)+','+(height+dy),
                  (dx+10)+','+(height+dy),
                  dx+','+dy
              ];
              return '<polygon id="im-'+id+'" points="'+points.join(' ')+'" fill="'+fill+'"'+atts+' />';
          case 'parlgrm-right':
              points = [
                  (dx+10)+","+dy,
                  (width+dx)+","+dy,
                  (width+dx-10)+","+(height+dy),
                  dx+","+(height+dy),
                  (dx+10)+","+dy
              ];
              return '<polygon id="im-'+id+'" points="'+points.join(' ')+'" fill="'+fill+'"'+atts+' />';
          case 'ellipse':
              return '<ellipse id="im'+id+'" cx="'+(dx+width/2)+'" cy="'+(dy+height/2)+'" rx="'+width/2+'" ry="'+height/2+'" fill="'+fill+'"'+atts+' />';
          case "rhombus":
              points = [
                  dx+","+(dy+height/2),
                  (dx+width/2)+","+dy,
                  (dx+width)+","+(dy+height/2),
                  (dx+width/2)+","+(dy+height),
                  dx+","+(dy+height/2)
              ];
              return '<polygon id="im-'+id+'" points="'+points.join(' ')+'" fill="'+fill+'"'+atts+' />';
          case "hex-hor":
              points = [
                  (dx+width/4)+","+dy,
                  (dx+width/4*3)+","+dy,
                  (dx+width)+","+(dy+height/2),
                  (dx+width/4*3)+","+(dy+height),
                  (dx+width/4)+","+(dy+height),
                  dx+","+(dy+height/2)
              ];
              return '<polygon id="im-'+id+'" points="'+points.join(' ')+'" fill="'+fill+'"'+atts+' />';
          case "hex-ver":
              points = [
                  dx+","+(dy+height/4),
                  dx+","+(dy+height/4*3),
                  (dx+width/2)+","+(dy+height),
                  (dx+width)+","+(dy+height/4*3),
                  (dx+width)+","+(dy+height/4),
                  (dx+width/2)+","+dy
              ];
              return '<polygon id="im-'+id+'" points="'+points.join(' ')+'" fill="'+fill+'"'+atts+' />';
      }
  }
  
  function get_filters(id, width, height, options) {
      var filters = get_filter(id, width, height, options);
      return filters;
  }
  
  function get_filter(id, width, height, options) {
        var filter = '\
<filter id="im-'+id+'-shadow">\
  <feGaussianBlur in="SourceAlpha" stdDeviation="'+options.shadow_size+'"/>\
  <feComponentTransfer>\
    <feFuncA type="linear" slope="'+options.shadow_opacity+'"/>\
  </feComponentTransfer>\
  <feMerge>\
    <feMergeNode/>\
    <feMergeNode in="SourceGraphic"/>\
  </feMerge>\
</filter>\
';
      return filter;
  }

})(jQuery);