<?php
/*
Plugin Name: Image masks
Plugin URI: http://github.com/kasparsj/image-masks
Description: Image masks
Author: Kaspars Jaudzems
Author URI: http://kasparsj.wordpress.com
Version: 1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class ImageMasks {
    
    static protected $masks = array("parlgrm-left", "parlgrm-right", "ellipse", "rhombus", "hex-hor", "hex-ver");
    static protected $masks_alt = array('parlgrm-left' => 'parlgrm-right', 'parlgrm-right' => 'parlgrm-left');
    static protected $instance = 0;
    static protected $defs = array();
    
    protected $options;
    
    public function __construct() {
        add_action('init', array($this, 'init'));
        add_action('admin_init', array($this, 'admin_init'));
        add_action('admin_menu', array($this, 'admin_menu'));
        add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
    }
    
    public function init() {
        $this->options = get_option('image_masks');

        $this->enabled = $this->options['mask'];
        if (!$this->enabled) {
            foreach ($this->options["th"] as $size => $config) {
                if ($config["mask"]) {
                    $this->enabled = true;
                    break;
                }
            }
        }
        if ($this->enabled)
            add_action('wp_enqueue_scripts', array($this, 'site_scripts'));
    
        if (($this->options['restore'] == 'on') || (!is_array($this->options))) {
            $this->options = array(
                "mask" => "",
                "size" => "thumbnail",
                "custom_width" => 144,
                "custom_height" => 125,
                "layout" => "",
                "spacing" => 5,
                "border" => array("size" => 0, "color" => "#000000"),
                "shadow" => array("size" => 0, "opacity" => 75, "dx" => 0, "dy" => 0),
                "th" => array(),
            );
            update_option('image_masks', $this->options);
        }
        else {
            if ($this->options["mask"])
                add_filter('post_gallery', array($this, 'post_gallery'), 1001, 2);

            if ($this->options["size"] == "im-gallery-thumb")
                add_image_size("im-gallery-thumb", $this->options['custom_width'], $this->options['custom_height']);
        }
    }

    public function site_scripts() {
        wp_enqueue_script( 'im-Modernizr', plugins_url('js/modernizr.custom.57883.js', __FILE__), array(), '1.0' );
        wp_enqueue_script( 'jquery.imageMask', plugins_url('js/jquery.imageMask.js', __FILE__), array('jquery'), '1.0', true );
        add_action('wp_footer', array($this, 'site_footer'));
    }

    public function site_footer() {
        $this->include_template("footer.php", array(
            'options' => $this->options
        ));
    }
    
    public function post_gallery($html, $attr = array()) {
        global $post;

        // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
        if ( isset( $attr['orderby'] ) ) {
            $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
            if ( !$attr['orderby'] )
                unset( $attr['orderby'] );
        }

        $default_float = is_rtl() ? 'right' : 'left';
        extract(shortcode_atts(array(
            'order'         => 'ASC',
            'orderby'       => 'menu_order ID',
            'id'            => $post->ID,
            'itemtag'       => 'dl',
            'icontag'       => 'dt',
            'captiontag'    => 'dd',
            'columns'       => 3, // it's hardcoded in WP
            'spacing'       => $this->options["spacing"],
            'border_size'   => $this->options["border"]["size"],
            'border_color'  => $this->options["border"]["color"],
            'shadow_size'   => $this->options["shadow"]["size"],
            'shadow_opacity' => $this->options["shadow"]["opacity"],
            'shadow_dx'     => $this->options["shadow"]["dx"],
            'shadow_dy'     => $this->options["shadow"]["dy"],
            'size'          => $this->options["size"],
            'include'       => '',
            'exclude'       => '',
            'float'         => $default_float,
            'link'          => '',
            'mask'          => $this->options["mask"],
            'layout'        => $this->options["layout"]
        ), $attr));

        /**
         * @var $order
         * @var $orderby
         * @var $id
         * @var $itemtag
         * @var $icontag
         * @var $captiontag
         * @var $columns
         * @var $spacing
         * @var $border_size
         * @var $border_color
         * @var $shadow_size
         * @var $shadow_opacity
         * @var $shadow_dx
         * @var $shadow_dy
         * @var $size
         * @var $float
         * @var $link
         * @var $mask
         * @var $layout
         */

        if ($float != 'left' && $float != 'right')
            $float = $default_float;

        $id = intval($id);
        if ( 'RAND' == $order )
            $orderby = 'none';

        if ( !empty($include) ) {
            $include = preg_replace( '/[^0-9,]+/', '', $include );
            $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

            $attachments = array();
            foreach ( $_attachments as $key => $val ) {
                $attachments[$val->ID] = $_attachments[$key];
            }
        } elseif ( !empty($exclude) ) {
            $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
            $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
        } else {
            $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
        }

        if ( empty($attachments) )
            return '';

        if ( is_feed() ) {
            $output = "\n";
            foreach ( $attachments as $att_id => $attachment )
                $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
            return $output;
        }

        $itemtag = tag_escape($itemtag);
        $captiontag = tag_escape($captiontag);
        $columns = intval(ceil($columns/2)*2);
        $spacing = intval($spacing);
        list($width, $height) = $this->get_dimensions($size);
        
        return $this->include_template("gallery.php", array(
            'id' => $id,
            'itemtag' => $itemtag,
            'columns' => $columns,
            'spacing' => $spacing,
            'border' => array("size" => $border_size, "color" => $border_color),
            'border_size' => $border_size,
            'shadow' => array("size" => $shadow_size, "opacity" => $shadow_opacity, "dx" => $shadow_dx, "dy" => $shadow_dy),
            'shadow_size' => $shadow_size,
            'size' => $size,
            'width' => $width,
            'height'=> $height,
            'total_width' => $width+$border_size*2+$shadow_size*2,
            'total_height' => $height+$border_size*2+$shadow_size*2,
            'float' => $float,
            'link' => $link,
            'mask' => $mask,
            'group_size' => ($layout ? 2 : 1),
            'layout' => $layout,
            'attachments' => array_values($attachments),
            'instance' => ++self::$instance,
            'fallback' => $html
        ), true);
    }

    public function admin_init() {
        register_setting('image_masks', 'image_masks', array($this, 'validate_options'));
    }
    
    public function admin_menu() {
        $page = add_options_page('Image Masks', 'Image Masks', 'manage_options', 'image_masks', array($this, 'options_page'));
    }
    
    public function admin_scripts($hook) {
        if ($hook == "settings_page_image_masks") {
            wp_enqueue_style( 'wp-color-picker' );
            wp_enqueue_script( 'wp-color-picker' );
        }
    }
    
    public function validate_options($input) {
        return $input;
    }
    
    public function options_page() {
        global $_wp_additional_image_sizes;
        $sizes = array();
        if (get_option('thumbnail_crop'))
            $sizes['thumbnail'] = 'thumbnail';
        foreach ($_wp_additional_image_sizes as $size => $options) {
            if ($options['crop'])
                $sizes[$size] = $size;
        }
        $this->include_template("options-page.php", array(
            'options' => $this->options,
            'masks' => self::$masks,
            'sizes' => $sizes
        ));
    }
    
    protected function get_dimensions($size) {
        if (is_array($size)) {
            return $size;
        }
        elseif ($size == 'thumbnail') {
            return array(
                get_option('thumbnail_size_w'),
                get_option('thumbnail_size_h'));
        }
        else {
            global $_wp_additional_image_sizes;
            return array(
                $_wp_additional_image_sizes[$size]['width'],
                $_wp_additional_image_sizes[$size]['height']);
        }
    }
    
    protected function get_defs($mask, $layout, $width, $height, $border, $shadow) {
        $id = $mask.'-'.$width.'x'.$height;
        if (!isset(self::$defs[$id])) {
            self::$defs[$id] = -1;
            $this->include_template("defs.php", array(
                'id' => $id,
                'mask' => $mask,
                'layout' => $layout,
                'width' => $width,
                'height' => $height,
                'border' => $border,
                'shadow' => $shadow
            ));
        }
        self::$defs[$id]++;
        if ($layout == 'alternately' && in_array($mask, self::$masks_alt) && self::$defs[$id]%2)
            $id .= '-alt';
        return $id;
    }
    
    protected function get_shapes($id, $mask, $layout, $width, $height, $border, $shadow) {
        $defs = $this->get_shape($id, $mask, $width, $height, $border, $shadow);
        if ($border)
            $defs .= $this->get_shape($id.'-border', $mask, $width, $height, $border, $shadow, true);
        if ($layout == "alternately" && isset(self::$masks_alt[$mask]))
            $defs .= $this->get_shapes($id.'-alt', self::$masks_alt[$mask], "", $width, $height, $border, $shadow);
        return $defs;
    }
    
    protected function get_shape($id, $mask, $width, $height, $border = false, $shadow = false, $is_border = false) {
        $dx = $dy = @$border["size"];
        $fill = "black";
        $atts = '';
        if ($is_border && @$border["size"]) {
            $dx = $dy = $border["size"]/2;
            $width += $border["size"]/2;
            $height += $border["size"]/2;
            $fill = "none";
            $atts .= sprintf(' stroke="%s" stroke-width="%d"', $border["color"], $border["size"]);
        }
        if ($shadow && @$shadow["size"]) {
            $dx += $shadow["size"];
            $dy += $shadow["size"];
        }
        switch ($mask) {
            case "ellipse":
                return sprintf('<ellipse id="im-%s" cx="%d" cy="%d" rx="%d" ry="%d" fill="%s"%s />',
                        $id, $width/2, $height/2+dx, $width/2+dy, $height/2, $fill, $atts);
            case "parlgrm-left":
                $points = array(
                    "$dx,$dy",
                    ($width+$dx-10).",".$dy,
                    ($width+$dx).",".($height+$dy),
                    ($dx+10).",".($height+$dy),
                    "$dx,$dy"
                );
                return sprintf('<polygon id="im-%s" points="%s" fill="%s"%s />',
                        $id, implode(' ', $points), $fill, $atts);
            case "parlgrm-right":
                $points = array(
                    ($dx+10).",".$dy,
                    ($width+$dx).",".$dy,
                    ($width+$dx-10).",".($height+$dy),
                    $dx.",".($height+$dy),
                    ($dx+10).",".$dy
                );
                return sprintf('<polygon id="im-%s" points="%s" fill="%s"%s />',
                        $id, implode(' ', $points), $fill, $atts);
            case "rhombus":
                $points = array(
                    $dx.",".($dy+$height/2),
                    ($dx+$width/2).",".$dy,
                    ($dx+$width).",".($dy+$height/2),
                    ($dx+$width/2).",".($dy+$height),
                    $dx.",".($dy+$height/2)
                );
                return sprintf('<polygon id="im-%s" points="%s" fill="%s"%s />',
                        $id, implode(' ', $points), $fill, $atts);
            case "hex-hor":
                $points = array(
                    ($dx+$width/4).",".$dy,
                    ($dx+$width/4*3).",".$dy,
                    ($dx+$width).",".($dy+$height/2),
                    ($dx+$width/4*3).",".($dy+$height),
                    ($dx+$width/4).",".($dy+$height),
                    $dx.",".($dy+$height/2)
                );
                return sprintf('<polygon id="im-%s" points="%s" fill="%s"%s />',
                        $id, implode(' ', $points), $fill, $atts);
            case "hex-ver":
                $points = array(
                    $dx.",".($dy+$height/4),
                    $dx.",".($dy+$height/4*3),
                    ($dx+$width/2).",".($dy+$height),
                    ($dx+$width).",".($dy+$height/4*3),
                    ($dx+$width).",".($dy+$height/4),
                    ($dx+$width/2).",".$dy
                );
                return sprintf('<polygon id="im-%s" points="%s" fill="%s"%s />',
                        $id, implode(' ', $points), $fill);
        }
    }
    
    protected function get_thumbnail($link, $attachmentID, $id, $src, $width, $height, $border, $shadow) {
        $image = $this->get_image($id, $src, $width, $height, $border, $shadow);
        if (isset($link))
            $image = $this->get_link($link, $attachmentID, $image);
        return $image;
    }
    
    protected function get_image($id, $src, $width, $height, $border, $shadow) {
        $image = sprintf('<image clip-path="url(#im-%s-clippath)" width="%d" height="%d" xlink:href="%s" x="%d" y="%d" />',
                $id, $width, $height, $src, $border["size"]+$shadow["size"], $border["size"]+$shadow["size"]);
        if (@$border["size"])
            $image .= sprintf('<use xlink:href="#im-%s-border" filter="url(#im-%s-shadow)" />',
                    $id, $id);
        return $image;
    }

    protected function get_link($link, $attachmentID, $content) {
        return sprintf('<a xlink:href="%s">%s</a>',
                'file' == $link ? wp_get_attachment_url($attachmentID) : get_attachment_link($attachmentID), $content);
    }
    
    protected function get_filters($id, $mask, $layout, $shadow) {
        $filters = $this->get_filter($id, $shadow);
        if ($layout == "alternately" && isset(self::$masks_alt[$mask]))
            $filters .= $this->get_filter($id.'-alt', $shadow);
        return $filters;
    }
    
    protected function get_filter($id, $shadow) {
        $format = <<<EOD
<filter id="im-%s-shadow">
  <feGaussianBlur in="SourceAlpha" stdDeviation="%s"/>
  <feOffset dx="%d" dy="%d" result="offsetblur"/>
  <feComponentTransfer>
    <feFuncA type="linear" slope="%f"/>
  </feComponentTransfer>
  <feMerge>
    <feMergeNode/>
    <feMergeNode in="SourceGraphic"/>
  </feMerge>
</filter>
EOD;
        return sprintf($format, $id, $shadow["size"], $shadow["dx"], $shadow["dy"], $shadow["opacity"] > 1 ? $shadow["opacity"]/100 : $shadow["opacity"]);
    }

    protected function include_template($template, $vars, $return = false) {
        if ($return) ob_start();
        extract($vars);
        include("templates/".$template);
        if ($return) return ob_get_clean();
    }
}

new ImageMasks;

?>
